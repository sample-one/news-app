const apiKey = 'fa138e53be4f40d791154479aef34216';

const cardContainer = document.querySelector(".cardContainer");
const searchBtn = document.getElementById('searchBtn');
const inputData = document.getElementById('inputData');
const politicsBtn = document.getElementById('politics');
const technologyBtn = document.getElementById('technology');
const sportsBtn = document.getElementById('sports');
const businessBtn = document.getElementById('business');





// Function to truncate text to a specific word count
const truncateTextByWords = (text, maxWords) => {
  const words = text.split(' ');
  return words.length > maxWords ? words.slice(0, maxWords).join(' ') + '...' : text;
};

// Function to fetch news based on input
const fetchNews = async (query) => {
  try {
    const response = await fetch(`https://newsapi.org/v2/everything?q=${query}&apiKey=${apiKey}`);
    const data = await response.json();

    // Clear previous results
    cardContainer.innerHTML = "";
    inputData.value = "";

    if (data.articles.length === 0) {
      alert('No articles found for this search term.');
    }

    data.articles.forEach((article) => {
      const card = document.createElement("div");
      card.classList.add("card");

      card.innerHTML = `
        <img src="${article.urlToImage}" alt="Article Image">
        <h3>${article.title}</h3>
        <p>${truncateTextByWords(article.description, 30)}</p>
      `;

      cardContainer.appendChild(card);
    });
  } catch (error) {
    console.error('Error fetching news:', error);
  }
};

// Initial load
window.addEventListener('load', () => fetchNews('india'));

// Event listeners for search and category buttons
searchBtn.addEventListener('click', () => fetchNews(inputData.value));
politicsBtn.addEventListener('click', () => fetchNews('politics'));
technologyBtn.addEventListener('click', () => fetchNews('technology'));
sportsBtn.addEventListener('click', () => fetchNews('sports'));
businessBtn.addEventListener('click', () => fetchNews('business'));
